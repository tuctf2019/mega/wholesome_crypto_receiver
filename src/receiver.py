#!/usr/bin/env python

import socket, sys

from select import select

from _thread import *

SERVER_MODE = False

HOST = ''
PORT = 8888

TIMEOUT = 30

MESSAGE = 'I LOVE POCKET MONSTERS'

FLAG = 'TUCTF{M4NY_ST3PS_3QU4LS_B1G_R3W4RD}'

INTRO = """
What's the secret message?

"""

OUTRO = """
Congratulations on solving the Mega Challenge!

We hope you enjoyed it!

Here's your flag:

"""

if SERVER_MODE:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')

    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        sys.stdout.write(f'Bind failed. {msg}\n\n')
        sys.stdout.flush()
        sys.exit()

    print('Socket bind complete')

    s.listen(10)
    print('Socket now listening')

def clienthread(conn):
    try:
        conn.sendall(INTRO.encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').strip('\n').upper()
        if data == MESSAGE:
            conn.sendall(f'\n{OUTRO}{FLAG}\n\n'.encode('utf-8'))
        else:
            conn.sendall('\nThat\'s wrong. Please try again.\n\n'.encode('utf-8'))

        conn.close()
    except (BrokenPipeError, ConnectionResetError):
        pass
    except socket.timeout:
        conn.send('\nYou idled too long.\n'.encode('utf-8'))

def timeoutfunc():
    sys.stdout.write('\nYou idled too long\n\n')
    sys.stdout.flush()
    sys.exit(0)

if SERVER_MODE:
    while True:
        try:
            conn, addr = s.accept()
            print('Connected with ' + addr[0] + ': ' + str(addr[1]))
            conn.settimeout(TIMEOUT)

            start_new_thread(clienthread, (conn,))
        except KeyboardInterrupt:
            break
    sys.stdout.write('Shutting down\n')
    sys.stdout.flush()
else:
    sys.stdout.write(INTRO)
    sys.stdout.flush()

    rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
    if rlist:
        data = sys.stdin.readline()
    else:
        timeoutfunc()

    data = data.strip('\n').upper()
    if data == MESSAGE:
        sys.stdout.write(f'\n{OUTRO}{FLAG}\n\n')
    else:
        sys.stdout.write('\nThat\'s wrong. Please try again.\n\n')
    sys.stdout.flush()
