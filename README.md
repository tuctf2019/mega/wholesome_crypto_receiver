# Wholesome Crypto Receiver

This is the server that receives the solution.

Desc: `Thought you might need some wholesome crypto.`

Prereq: `rop me like a hurricane`

Flag: `TUCTF{M4NY_ST3PS_3QU4LS_B1G_R3W4RD}`
